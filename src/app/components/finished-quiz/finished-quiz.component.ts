import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-finished-quiz',
  templateUrl: './finished-quiz.component.html',
  styleUrls: ['./finished-quiz.component.css']
})
export class FinishedQuizComponent implements OnInit {

  countCorrect: number;
  questionNum: number;
  percentage: number;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.countCorrect = parseInt(this.route.snapshot.paramMap.get('countCorrect'), 10);
    this.questionNum = parseInt(this.route.snapshot.paramMap.get('questionNum'), 10);

    console.log(this.countCorrect);
    console.log(this.questionNum);
  }

  backToHome() {
    this.router.navigate(['/home']);
  }

  ngOnInit() {
    this.percentage = (this.countCorrect / this.questionNum) * 100;
    this.percentage = parseFloat(Number(this.percentage).toFixed(2));
  }

}
