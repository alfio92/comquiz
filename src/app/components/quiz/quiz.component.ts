import { QuizQuestion } from './../../models/quiz-question';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  fileJson: string;
  questionNum: number;                              // numero di domande da processare
  questionIndex = 0;
  questions: QuizQuestion[] = [];                   // domande del quiz
  currentQuestion: QuizQuestion;                    // domanda corrente
  lastQuestion = false;                             // ultima domanda
  correct: number = 0;

  constructor(private route: ActivatedRoute, private router: Router, private quizService: QuestionService) {
    this.fileJson = this.route.snapshot.paramMap.get('jsonPath');
    this.questionNum = parseInt(this.route.snapshot.paramMap.get('questionsNum'), 10);
  }

  ngOnInit() {

    this.quizService.getJSON(this.fileJson)
      .subscribe(
        result => {
          this.getRandomQuestions(result);
          this.questions.forEach(question => {

            question.responses.forEach(res => {
              res.userRes = false;
            });

          });
        },
        err => console.log(err)
      );

  }

  /**
   * Randomizzazione ordine delle domande
   */
  getRandomQuestions(allQuestions) {
    const duplicates = [];
    for ( let i = 0; i < this.questionNum; i++ ) {
      const index = this.quizService.getRandomIntNoDuplicates(0, allQuestions.questions.length - 1, duplicates);
      this.questions.push(allQuestions.questions[index]);
      this.getRandomResponses(allQuestions.questions[index]);
    }
    this.selectQuestion(this.questions[this.questionIndex]);
  }

  /**
   * Randomizzazione ordine delle risposte
   */
  getRandomResponses(question) {
    const responses = [];
    const duplicates = [];

    question.responses.forEach(() => {
      const index = this.quizService.getRandomIntNoDuplicates(0, question.responses.length - 1, duplicates);
      responses.push(question.responses[index]);
    });
    question.responses = responses;
  }

  selectQuestion(question) {
    this.currentQuestion = question;
  }

  checkQuizInput(event, index) {

    this.currentQuestion.responses[index].userRes =  !this.currentQuestion.responses[index].userRes;

  }

  showResponse() {
    const checkboxes = document.querySelectorAll('input[type=checkbox]');
    this.currentQuestion.responses.forEach( (res, index) => {
        if ( res.isCorrect && res.userRes ) {
          checkboxes[index].classList.add('is-valid');
        } else {
          if ( !res.isCorrect && res.userRes ) {
            checkboxes[index].classList.add('is-invalid');
          }
        }
    });
  }

  /**
   * Conta il numero di risposte corrette
   */
  countCorrectAnswers() {

    let countCorrectAns = 0;
    this.questions.forEach(question => {

      let correctAnswered = true;
      question.responses.forEach(response => {
        if ( response.isCorrect &&  !(response.isCorrect === response.userRes) ) {
          correctAnswered = false;
        }
      });
      correctAnswered ? ++countCorrectAns : '' ;
    });

    return countCorrectAns;
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  endQuiz() {
    this.router.navigate(['/finish', { countCorrect: this.countCorrectAnswers(), questionNum: this.questionNum }]);
  }

  /**
   * Controllo ultima domanda
   */
  checkLastQuestion() {
    this.lastQuestion = (this.questionIndex === this.questions.length - 1);
  }

  goAway() {
    if ( this.questionIndex < this.questionNum ) {
      this.selectQuestion(this.questions[++this.questionIndex]);
    }

    this.lastQuestion ? this.endQuiz() : '';
    this.checkLastQuestion();         // controllo ultima domanda
  }

  goBack() {
    if ( this.questionIndex > 0 ) {
      this.selectQuestion(this.questions[--this.questionIndex]);
    }
    this.checkLastQuestion();       // controllo ultima domanda
  }

}
