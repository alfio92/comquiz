import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DEFAULT_QUESTIONS_NUM } from './../../models/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  questionsNum: number = DEFAULT_QUESTIONS_NUM;

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  getJsonFile(event) {
    let path = event.target.files[0].name;
    console.log(event.target);
    console.log(event.target.value);
    console.log(event.target.files);
    console.log(event.target.files[0]);
    console.log(path);
    let data = URL.createObjectURL(event.target.files[0]);
    console.log(data);
    this.router.navigate(['/quiz', { jsonPath: data, questionsNum: this.questionsNum }]);
  }

  checkInput(event) {
    const num: number = parseInt(event.target.value, 10);   // cast a intero
    if ( !isNaN(num) && (num < 1 || num > 65) ) {
      event.preventDefault();
      this.questionsNum = DEFAULT_QUESTIONS_NUM;
    } else {
      this.questionsNum = num;
    }
  }

}
