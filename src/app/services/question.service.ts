import { QuizQuestion } from './../models/quiz-question';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, range, interval } from 'rxjs';
import { concatMap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  getJSON(path): Observable<QuizQuestion[]> {
    return this.http.get<QuizQuestion[]>(path);
  }

  getRandomNumbers() {

    const nums = [];
    for (let index = 0; index < 65; index++) {
      nums.push(index);
    }

  }

  inArray(arr, el) {
    for (let i = 0 ; i < arr.length; i++) {
      if (arr[i] === el) {
        return true;
      }
    }
    return false;
  }

  getRandomIntNoDuplicates(min, max, DuplicateArr) {
    const RandomInt = Math.floor(Math.random() * (max - min + 1)) + min;
    if ( DuplicateArr.length > ( max - min ) ){
      return false;  // break endless recursion
    }
    if (!this.inArray(DuplicateArr, RandomInt)) {
       DuplicateArr.push(RandomInt);
       return RandomInt;
    }
    return this.getRandomIntNoDuplicates(min, max, DuplicateArr); // recurse
  }

}
