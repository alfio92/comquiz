
import { QuizResponse } from './quiz-response';

export class QuizQuestion {

  id: number;
  qtaResponses: number;             // numero di risposte corrette
  question: string;                 // domanda
  responses: QuizResponse[] = [];   // risposte
  correctAnswered: boolean;

}
