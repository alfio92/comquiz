const electron = require('electron');

const { app, BrowserWindow, Menu } = electron;  // con l'oggetto Menu possiamo andare a ristrutturare il menu dell'applicazione

let mainWindow; // finestra di home

app.on('ready', () => {
    mainWindow = new BrowserWindow({width: 800, height: 600, icon: __dirname + '/dist/angular-electron-app/assets/ico.ico'});
    mainWindow.loadURL('file://' + __dirname + '/dist/angular-electron-app/index.html');
    mainWindow.on('closed', () => app.quit());  // alla chiusura della mainWindow mi chiude tutte le altre finstre dell'applicazione

    const mainMenu = Menu.buildFromTemplate(menuTemplate);  // crea un nuovo menu
    Menu.setApplicationMenu(mainMenu);                      // sostituisce il menu di default con quello già definito
});

// da qui avremo un solo dropdown nel menu
const menuTemplate = [
    // {}, aggiungerlo per la versione mac
    {
        label: 'View',
        submenu: [
            {
                label: 'Quit',  // se si clicca tale label nel menu, chiudo l'app
                accelerator:  process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q',  // se si preme ctrl + Q l'app verrà chiusa
                click() {
                  app.quit();
                }
            },

        ]
    }
];

if ( process.platform === 'darwin') {    // vedo se l'OS è darwin (ovvero MacOS) (tramite NodeJS)
    menuTemplate.unshift({}); // allora inserisco il menu customizzato saltando di una cella nel menu tab (la prima cella nel mac è nel nome dell'applicazione)
}

if ( process.env.NODE_ENV !==  'production' ) { // se l'ambiente di esecuzione JS è diverso dalla fase di produzione
    // production
    // development
    // statging
    // test
    // menuTemplate.push({ // creiamo una nuova entry nel menu
    //     label: 'DEV',
    //     submenu: [
    //       {
    //           label: 'Toggle Dev Tool',  // se si clicca tale label nel menu, chiudo l'app
    //           click() {
    //             mainWindow.webContents.toggleDevTools();
    //           }
    //       },

    //   ]
    // });

}
